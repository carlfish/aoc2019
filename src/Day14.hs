module Day14 where

import Data.Map.Strict (Map, fromList, size)

import qualified Data.Attoparsec.ByteString.Char8 as AP
import Data.ByteString (ByteString)
import Lib (onePerLine, whitespace, commaSeparated, runFile')

newtype Chemical = Chemical ByteString deriving (Eq, Ord, Show)

type Ingredient = (Chemical, Int)
type Ingredients = [ Ingredient ]
data Recipe = Recipe Ingredients Ingredient
type RecipeBook = Map Chemical Recipe

-- simplify :: RecipeBook -> Ingredients -> Ingredients
-- simplify book ingredients = foldl 


-- Exercises

ex1 :: IO (Either String Int)
ex1 = runFile' "data/day14/1401.txt" parser size

-- Parsers

parseChemical :: AP.Parser Chemical
parseChemical = Chemical <$> (AP.takeWhile (AP.inClass ['A'..'Z']))

parseComponent :: AP.Parser (Chemical, Int)
parseComponent = (flip (,)) <$> AP.decimal <*> (whitespace *> parseChemical)

parseIngredients :: AP.Parser Ingredients
parseIngredients = commaSeparated parseComponent

parseRecipe :: AP.Parser Recipe
parseRecipe = Recipe <$> parseIngredients <*> (whitespace *> "=>" *> whitespace *> parseComponent)

parser :: AP.Parser RecipeBook
parser =  fromList <$> onePerLine ((\r@(Recipe _ (c, _)) -> (c, r)) <$> parseRecipe)

